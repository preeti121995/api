# Generated by Django 3.2.5 on 2021-07-22 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis_crud', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='on_deleted',
            field=models.BooleanField(default=True),
        ),
    ]
