import json
from django.urls import reverse
from django.conf import settings
from rest_framework.test import APITestCase, APIClient
from rest_framework import status
import requests
from apis_crud.models import *


class TestCase(APITestCase):
    """This class is used to setup the authentication for all apis."""
    def setUp(self):
        self.client = APIClient()
        
class UserViewTestCases(TestCase):
    """This class is used to test usersView API."""
    def setUp(self):
    #     """This function is used for setup databases."""
    #     super().setUp()
        self.user = Users.objects.create(
            name="test",
            email="testing@gmail.com",
            phone="1234567890",
            address="delhi"
        )

    def test_users_list_success(self):
        """Testcase for get category list success."""
        url = reverse('users-list')
        response = self.client.get(path=url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
