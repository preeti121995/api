# from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework import viewsets
from apis_crud.models import Users
from apis_crud.serializers import UserSerializer

class CrudView(viewsets.ModelViewSet):
    """
    This is a view for categories.
    """
    queryset = Users.objects.all()
    serializer_class = UserSerializer
    http_method_names = ['get','post','put','delete']
    filter_backends = (filters.DjangoFilterBackend, SearchFilter )
    # filter_backends = [filters.SearchFilter,]
    search_fields = ['name', 'email','phone']