from rest_framework import serializers
from apis_crud.models import Users



class UserSerializer(serializers.ModelSerializer):
    """This serializers is for users."""
    class Meta:
        model = Users
        fields = '__all__' 