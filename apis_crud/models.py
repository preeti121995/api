from django.db import models

# Create your models here.

class Users(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=15)
    address = models.CharField(max_length=500)
    on_deleted=models.BooleanField(default=True)


    def __str__(self):
        return self.name