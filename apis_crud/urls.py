from django.urls import path
from rest_framework import routers

from apis_crud.views import CrudView

router =  routers.SimpleRouter(trailing_slash=False)

router.register(r'crud/api',CrudView)

urlpatterns = [
    
]


urlpatterns += router.urls
