### Python: 3.6.x (64 bit)
### Framework: Django 3.x, DjangoRestFramework 3.11

### Clone Repository:
$ git clone https://preeti121995@bitbucket.org/preeti121995/api.git

### Create & Activate Virtualenv:
$ cd <app_root>
$ python3 -m venv testenv
$ source testenv/bin/activate
### or:
py -m venv testenv
.\testenv\Scripts\activate



### Install Dependencies:
(testenv) $ pip install -r requirements.txt

### Run DB migrations:
(testenv) $ python manage.py makemigrations
(testenv) $ python manage.py migrate

### Create Initial Superadmin Credentials:
(testenv) $ python manage.py createsuperuser
-- then provide superuser credentials as prompted.

### Run the server:
(testenv) $ python manage.py runserver
